Kibana
=============

This role's duty is to install [Kibana](https://www.elastic.co/products/kibana).

Variables
---------

### `kibana_version`
The version number of Kibana release to be installed. By default, version **4.4.1** is installed.

### `kibana_checksum`
If set, the downloaded .TAR.GZ file is tested against this checksum.

### `kibana_config`
A dictionary of Kibana configuration to be written in `kibana.yml` config file. By default, no configuration is written.

### `kibana_plugins`
A dictionary of Kibana plugins to be installed. For each item in the dictionary, the key represents the plugin name, while the value represents the full string to be passed to `kibana/bin/kibana plugin --install` command. By default, no plugin is installed.

Example
-------

```yaml
---
kibana_config:
  elasticsearch.url: http://127.0.0.1:9200

kibana_plugins:
  marvel: elasticsearch/marvel/latest
```
